# Flckr

This gem makes a collage for given keywords from Flickr.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'flckr'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install flckr

## Usage

Getting help

```bash
flckrcli help
```

Create a collage

```bash
flckrcli create FILENAME -a, --api-key=API_KEY -k, --keywords=one two three  # Make a collage for given keywords from Flick
```

Example

```bash
flckrcli collage my_collage -k porshe lexus honda ford volkswagen mercedes audi honda ferrari maserati -a bb8fa6cd487eb07a509356acd2fe6113
```


## Development

After checking out the repo, run `bundle` to install dependencies. Then, run `rake spec` to run the tests. You can also run `exe/flckrcli` for an interactive prompt that will allow you to experiment.
To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on BitBucket at https://bitbucket.org/pozitive/flckr. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

