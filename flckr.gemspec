# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'flckr/version'

Gem::Specification.new do |spec|
  spec.name          = 'flckr'
  spec.version       = Flckr::VERSION
  spec.authors       = ['Dima Sukhikh']
  spec.email         = ['coddeys@gmail.com']

  spec.summary       = 'Create collage from 10 top-rated Flickr images'
  spec.homepage      = 'https://bitbucket.org/pozitive/flckr'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end

  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'thor', '~> 0.19.1'
  spec.add_runtime_dependency 'faraday', '~> 0.10.0'
  spec.add_runtime_dependency 'faraday_middleware', '~> 0.10.0'
  spec.add_runtime_dependency 'typhoeus', '~> 1.1.0', '>= 1.1.0'
  spec.add_runtime_dependency 'rmagick-screwdrivers', '~> 0.2.0'

  spec.add_development_dependency 'bundler', '~> 1.13'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'vcr', '~> 3.0.3', '>= 3.0.3'
  spec.add_development_dependency 'webmock', '~> 2.1.0', '>= 2.1.0'
end
