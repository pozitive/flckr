require 'flckr/version'

module Flckr
  def self.silent_warnings
    old_stderr = $stderr
    $stderr = StringIO.new
    yield
  ensure
    $stderr = old_stderr
  end

  def self.collage(_keywords, _name)
    true
  end
end
