module Flckr
  module Dict
    DEFAULT_DICT = '/usr/share/dict/words'.freeze
    def self.random_line
      File.readlines(DEFAULT_DICT).sample.strip
    end

    def self.make_ten_words(words)
      ten_words = words.take(10)
      count = ten_words.count
      return ten_words if count == 10
      (10 - count).times { ten_words << Dict.random_line }
      ten_words
    end
  end
end
