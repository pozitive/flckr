Flckr.silent_warnings do
  require 'rmagick/screwdrivers'
end

module Flckr
  module Collage
    def self.create(dir, name)
      options = {
        columns: 5,
        thumb_width: 100
      }
      Magick::Screwdrivers.collage(dir, options).write("#{name}.jpeg")
    end
  end
end
