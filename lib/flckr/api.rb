require_relative 'dict'
require 'json'
require 'faraday'
require 'typhoeus'
require 'typhoeus/adapters/faraday'
require 'securerandom'

module Flckr
  class Api
    def initialize(api_key)
      @api_key = api_key
    end

    def download_by(keywords, dir)
      ten_keywords = Dict.make_ten_words keywords
      urls = search_all ten_keywords
      urls.map do |url|
        download(url, dir)
      end
    end

    def download(url, dir)
      response = conn.get url
      File.open("#{dir}/#{SecureRandom.hex(5)}.jpg", 'wb') { |fp| fp.write(response.body) }
    end

    def generate_url(photo_id:, server_id:, farm_id:, secret:)
      "https://farm#{farm_id}.staticflickr.com/#{server_id}/#{photo_id}_#{secret}_m.jpg"
    end

    def search_all(keywords)
      search(keywords)
        .map { |r| parse r }
        .reject(&:nil?)
        .map { |e| generate_url e }
    end

    def search(keywords)
      [].tap do |responses|
        client = conn
        client.in_parallel do
          keywords.each do |keyword|
            responses << client.get('/services/rest', params(@api_key, keyword))
          end
        end
      end
    end

    private

    def parse(response)
      hash = JSON.parse response.body
      return $stderr.puts hash['message'] if hash['stat'] == 'fail'
      photo = hash['photos']['photo'].first
      to_hash photo unless photo.nil?
    end

    def to_hash(photo)
      { photo_id: photo['id'],
        farm_id: photo['farm'],
        secret: photo['secret'],
        server_id: photo['server'] }
    end

    def conn
      Faraday.new(url: 'https://api.flickr.com') do |faraday|
        faraday.adapter :typhoeus
      end
    end

    def params(api_key, keyword)
      { method: 'flickr.photos.search',
        api_key: api_key,
        text: keyword,
        format: 'json',
        nojsoncallback:  1,
        page: 1,
        per_page: 1 }
    end
  end
end
