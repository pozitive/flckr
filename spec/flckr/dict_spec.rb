require 'spec_helper'
require 'vcr'

describe Flckr::Dict do
  describe '#make_ten_words' do
    it { expect(described_class.make_ten_words([*'a'..'c']).count).to eq 10 }
    it { expect(described_class.make_ten_words([*'a'..'j']).count).to eq 10 }
    it { expect(described_class.make_ten_words([*'a'..'q']).count).to eq 10 }
  end
end
