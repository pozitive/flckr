require 'spec_helper'
require 'vcr'

describe Flckr::Api do
  let(:api) { described_class.new('bb8fa6cd487eb07a509356acd2fe6113') }
  let(:api_failed) { described_class.new('fail_token') }

  describe '#generate_url' do
    let(:hash) do
      { photo_id: '30980951490', server_id: '5449', farm_id: 6, secret: 'a4e7538588' }
    end
    it { expect(api.generate_url(hash)).to eq 'https://farm6.staticflickr.com/5449/30980951490_a4e7538588_m.jpg' }
  end

  describe '#search' do
    context 'succesfully' do
      before(:example) do
        VCR.use_cassette('api/search/succesfully', record: :once) do
          @search = api.search(['auto_collage'])
        end
      end

      it { expect(JSON.parse(@search.first.body)['stat']).to eq('ok') }
      it { expect(JSON.parse(@search.first.body)['photos']['photo'].first['id']).to eq('30980951490') }
    end

    context 'unsuccesfully' do
      before(:example) do
        VCR.use_cassette('api/search/unsuccesfully', record: :once) do
          @search = api_failed.search(['auto_collage'])
        end
      end

      it { expect(JSON.parse(@search.first.body)['stat']).to eq('fail') }
      it { expect(JSON.parse(@search.first.body)['message']).to eq('Invalid API Key (Key has invalid format)') }
    end
  end

  describe '#search_all' do
    context 'succesfully' do
      let(:results) do
        ['https://farm6.staticflickr.com/5449/30980951490_a4e7538588_m.jpg',
         'https://farm1.staticflickr.com/72/31284245430_67194c0fe3_m.jpg',
         'https://farm1.staticflickr.com/148/31614917806_60524f46e5_m.jpg']
      end

      before(:example) do
        VCR.use_cassette('api/search_all/succesfully', record: :once) do
          @search_all = api.search_all(%w(auto_collage auto icecream))
        end
      end

      it { expect(@search_all).to eq results }
    end

    context 'unsuccesfully' do
      before(:example) do
        VCR.use_cassette('api/search_all/unsuccesfully', record: :once) do
          @search_all = api_failed.search_all(%w(auto_collage auto icecream))
        end
      end

      it { expect(@search_all).to eq([]) }
    end
  end
end
