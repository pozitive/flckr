$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'flckr'
require 'flckr/api'
require 'vcr'

VCR.configure do |config|
  config.cassette_library_dir = 'spec/fixtures/vcr_cassettes'
  config.hook_into :webmock # or :fakeweb
end
