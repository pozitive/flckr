require 'spec_helper'

describe Flckr do
  it 'has a version number' do
    expect(Flckr::VERSION).not_to be nil
  end

  describe '#collage' do
    it { expect(described_class.collage(['car'], 'auto_collage')).to eq(true) }
  end
end
